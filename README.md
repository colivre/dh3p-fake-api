# INSTALL

npm install -g json-server

# RUNNING

json-server --watch db.json --port 5000 --routes routes.json -H *your ip* --middlewares ./server.js

